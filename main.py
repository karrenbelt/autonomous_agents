
from src.message_handler import FilterMessagesByWordAndPrint
from src.behavior import GenerateRandom2WordMessage, SendOutgoingMessages, CheckIncomingMessages
from src.agent import AutonomousAgent
from src.environment import Environment


def setup_agent(name: str):

    agent = AutonomousAgent(name)

    # create message handlers
    message_handler = FilterMessagesByWordAndPrint("hello")

    # create behaviors
    words = ["hello", "sun", "world", "space", "moon", "crypto", "sky", "ocean", "universe", "human"]
    create_spam = GenerateRandom2WordMessage(vocabulary=words)
    send_messages = SendOutgoingMessages()
    check_messages = CheckIncomingMessages()

    # register
    agent.register_message_handler(message_handler)
    agent.register_behavior(create_spam)
    agent.register_behavior(send_messages)
    agent.register_behavior(check_messages)

    return agent


if __name__ == '__main__':

    # create environment
    environment = Environment()

    # create agents
    names = ['Alice', 'Bob', 'Charlie']
    agents = [setup_agent(name) for name in names]

    # activate autonomous agent system
    environment.activate()

    # deactivate with KeyBoardInterrupt: ctrl + c
