
# Autonomous Agents
Assignment is found [here](doc/Python%20Software%20Engineer%20-%20Project%20A.pdf)

The implementation can be executed by running:

```python3.8 main.py```


##  Environment

The environment in which autonomous agents exist and in which they can interact with other autonomous agents.
Chosen for a singleton so that all agents are guaranteed to be part of the same environment. 
It furthermore allows to register the agents automatically upon their instantiation.
Upon activation of the environment all agent tasks will be scheduled for execution until a ```KeyBoardInterrupt```.
When a KeyBoardInterrupt takes place the remaining scheduled tasks are cancelled during clean up.


## Autonomous Agents

I chose not to implement an abstract base class for the agent at this time for the simple reason that 
all attributes, methods and properties would be shared by the concrete class that would inherit them. 
If different types of agents would be required in the future this would be a good idea.

The concrete autonomous agent must have the following two behaviors:

1. generate random 2-word messages from an alphabet of 10 words every 2 seconds
2. filter messages for the keyword "hello" and if so print the whole message to stdout

Based on this desired behavior it is fair to assume that the randomly created messages need to be sent to another agent, 
such that an interaction between the agents is established. Hence, a third behavior can be inferred:

3. send the randomly generated message to another agent

Aside from behaviors the agent also needs to be able to register message handlers that it can apply. 
Both of these registrations are handled by appending them to a list of said instances.

The inbox and outbox of each agent are asynchronous Queue's allowing operations on them to be processed concurrently.


## Behavior:

Behavior is defined as an activity an agent can engage in. 

An abstract base class defines an interface that ensures each behavior is an asynchronous callable, which is used to 
express or execute the behavior. It also ensures that the behavior must be registered to an agent after it is 
instantiated before it can be executed.

The following three concrete classes are defined:
- One that generates random 2 word messages from a vocabulary of words. It also addresses this message to another
agent chosen at random from the environment and prepares for delivery by adding it to its outbox
- One that sends messages in its outbox to the agent in its environment it is addressed to.
- One that checks if messages have been received and responds by printing them out if they contain the keyword "hello"

The latter two behaviors are awaited with a timeout such that the loop doesn't deadlock in case an agent does 
not find any messages to be processed in either the inbox or outbox. 


## Message Handlers

An abstract base class defines that all message handlers need to be implemented as asynchronous callables that take a
message as its argument. This implementation is consistent with the way behaviors are implemented and in case message 
handlers are defined that require considerable work this will expedite the process of message handling.

A concrete class is implemented that filters messages based on a particular word in their content and prints out the 
whole message if there is a match.


## Messages

Messages are implemented as a dataclass. This was chosen because that is exactly what they contain, and it removes the 
need to write a lot of boilerplate code. One feature of dataclasses that is used here is freezing them after 
instantiation such that accidental modification cannot occur. The data they contain it a message type, author, 
recipient, content and a timestamp. The datatype can be used by messages handlers to filter out messages they need to 
operate on.


# Tests

- One simple unit test: [test_message.py](tests/test_message.py)

- One small integration test: [test_integration.py](tests/test_integration.py)

run as follows:

``` python3.8 -m unittest discover tests/```


# Potential improvements and further development

Strongly depends on future needs. Some ideas:

- Composable message handlers (e.g. using a combinator pattern, combining ```FilterByWord``` and ```Print```)
- Behavior could be extended to include additional base classes for one-time and recurrent behaviors.
- Dynamically adding and removing agents to and from the environment.
- Dynamically adding and removing behaviors to agents
- Encryption of messages. Note that public-private key encryption is not part of the python standard library.
Library such as [rsa](https://pypi.org/project/rsa/) or [bcrypt](https://pypi.org/project/bcrypt/) might be used.
- Message persistence. Currently, messages are produced and consumed after which garbage collection takes place, 
instead it might be favorable to store a copy of the messages for some period of time. This could for example be done 
as part of the logging process with the use of a RotatingFileHandler.
- swap out asyncio for [uvloop](https://pypi.org/project/uvloop/) for speed up. Using uvloop is supposedly as simple as 
switching the event loop policy:

```
import uvloop
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy)
```



