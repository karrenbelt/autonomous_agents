
from enum import Enum
from datetime import datetime
from dataclasses import dataclass


class MessageType(Enum):
    SPAM = 0
    REQUEST = 1
    PROPOSAL = 2
    ACCEPT = 3
    DECLINE = 4
    ERROR = 5


@dataclass(frozen=True)
class Message:
    type: MessageType
    author: str  # hex
    recipient: str  # hex
    content: str
    time: datetime = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

    def __repr__(self):
        return f"<{self.__class__.__name__} | {self.time} | {self.author} | {self.type}>"

    def __str__(self):
        return f"From: {self.author}\nTo: {self.recipient}\nType: {self.type}\nContent: {self.content}"
