
import time
import asyncio
import signal
import logging
import functools
from contextlib import suppress

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Singleton(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Environment(metaclass=Singleton):

    def __init__(self):
        self.agents = {}
        self.event_loop = asyncio.get_event_loop()

    async def run(self):
        tasks = [task for agent in self.agents.values() for task in agent.tasks]
        await asyncio.gather(*tasks)

    def activate(self):
        if self.event_loop.is_closed():
            self.event_loop = asyncio.new_event_loop()
            asyncio.set_event_loop(self.event_loop)

        signal_handler = functools.partial(asyncio.ensure_future, self._shutdown(signal.SIGINT))
        self.event_loop.add_signal_handler(signal.SIGINT, signal_handler)

        logging.info('starting up, scheduling tasks...')
        try:
            while True:
                t_start = time.time()
                self.event_loop.run_until_complete(self.run())
                logging.info(f'Cycle time elapsed: {time.time() - t_start}')
        except asyncio.CancelledError:
            logging.info('Environment deactivated')
        finally:
            # self.agents.clear()
            self.event_loop.close()

    async def _shutdown(self, sig):
        logging.info(f'received stop signal {sig.name}, cancelling tasks...')
        tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
        [task.cancel() for task in tasks]
        with suppress(asyncio.CancelledError):
            await asyncio.gather(*tasks)
        logging.info(f'finished awaiting cancelled tasks')
        self.event_loop.stop()

    def __repr__(self):
        return f"<{self.__class__.__name__} with {len(self.agents)} Agents>"
