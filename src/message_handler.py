
from abc import ABC, abstractmethod
from src.message import Message


class AbstractMessageHandler(ABC):

    @abstractmethod
    async def __call__(self, message: Message):
        raise NotImplementedError


class FilterMessagesByWordAndPrint(AbstractMessageHandler):

    def __init__(self, word: str):
        self.word = word

    async def __call__(self, message: Message):
        if self.word in set(message.content.split()):  # if "hello" we don't want to select e.g. othello, phelloderms
            print(message)
