
import asyncio

from src.message_handler import AbstractMessageHandler
from src.behavior import AbstractBehavior
from src.environment import Environment


class AutonomousAgent:

    environment = Environment()

    def __init__(self, name: str = ""):
        self.name = name
        self.inbox = asyncio.Queue(maxsize=100)  # will block until the the agent pulls some items out
        self.outbox = asyncio.Queue(maxsize=100)
        self._behaviors = []
        self._message_handlers = []
        self.environment.agents[self.id] = self
        # self.password = ''.join(secrets.choice(string.ascii_letters + string.digits) for _ in range(24))

    def register_behavior(self, behavior: AbstractBehavior, *args, **kwargs):
        """any *args and **kwargs will be passed onto the behavior when tasks are scheduled"""
        behavior.agent = self
        self._behaviors.append((behavior, args, kwargs))

    def register_message_handler(self, message_handler: AbstractMessageHandler):
        self._message_handlers.append(message_handler)

    @property
    def id(self):
        return hex(id(self))

    @property
    def behaviors(self):
        return self._behaviors

    @property
    def message_handlers(self):
        return self._message_handlers

    @property
    def other_agents(self):
        return [agent for agent in self.environment.agents.values() if agent is not self]

    @property
    def loop(self):
        return self.environment.event_loop

    @property
    def tasks(self):
        return [self.loop.create_task(behavior(*args, **kwargs)) for (behavior, args, kwargs) in self._behaviors]

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self.name or self.id}>"
