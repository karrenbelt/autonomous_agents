
from abc import ABC, abstractmethod
from typing import List
import asyncio
import random

from src.message import Message, MessageType


class RegistrationError(Exception):
    pass


class AbstractBehavior(ABC):
    
    def __init__(self):
        self._agent = None
    
    @abstractmethod
    async def __call__(self, *args, **kwargs):
        """express behavior"""
        raise NotImplementedError

    @property
    def agent(self):
        if not self._agent:
            raise RegistrationError("Behavior not registered to an Agent")
        return self._agent
    
    @agent.setter
    def agent(self, agent):
        self._agent = agent        


class GenerateRandom2WordMessage(AbstractBehavior):

    def __init__(self, vocabulary: List[str]):
        assert len(vocabulary) >= 2, "a minimum of two words is required"
        super().__init__()
        self.vocabulary = vocabulary

    async def __call__(self):
        if not self.agent.other_agents:
            raise RegistrationError("No other Agents registered to the Environment")
        recipient = random.choice(self.agent.other_agents)
        content = ' '.join(random.sample(self.vocabulary, 2))
        message = Message(type=MessageType.SPAM, author=self.agent.id, recipient=recipient.id, content=content)
        await self.agent.outbox.put(message)
        await asyncio.sleep(2)


class SendOutgoingMessages(AbstractBehavior):

    async def __call__(self):
        try:
            message = await asyncio.wait_for(self.agent.outbox.get(), timeout=1)
        except asyncio.TimeoutError:
            message = None

        if message:
            recipient = self.agent.environment.agents.get(message.recipient)
            if not recipient:
                raise RegistrationError("Agent not registered to the Environment")
            await recipient.inbox.put(message)


class CheckIncomingMessages(AbstractBehavior):

    async def __call__(self):
        try:
            message = await asyncio.wait_for(self.agent.inbox.get(), timeout=1)
        except asyncio.TimeoutError:
            message = None

        if message:
            for message_handler in self.agent.message_handlers:
                await message_handler(message)
