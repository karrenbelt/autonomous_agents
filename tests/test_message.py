
import unittest
from datetime import datetime

from src.message import Message, MessageType


class TestMessageAttributes(unittest.TestCase):

    def setUp(self):
        self.message = Message(MessageType.SPAM, hex(id(True)), hex(id(False)), "hello world")

    def test_type(self):
        self.assertEqual(self.message.type, MessageType.SPAM)

    def test_author(self):
        self.assertEqual(self.message.author, hex(id(True)))

    def test_recipient(self):
        self.assertEqual(self.message.recipient, hex(id(False)))

    def test_content(self):
        self.assertEqual(self.message.content, "hello world")

    def test_time_format(self):
        timestamp = datetime.strptime(self.message.time, "%d/%m/%Y %H:%M:%S")
        self.assertIsInstance(timestamp, datetime)

    def tearDown(self):
        pass
