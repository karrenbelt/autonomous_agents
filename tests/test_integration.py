
import unittest
from src.message_handler import FilterMessagesByWordAndPrint
from src.behavior import GenerateRandom2WordMessage, SendOutgoingMessages, CheckIncomingMessages
from src.agent import AutonomousAgent
from src.environment import Environment


class TestTwoAgentEnvironment(unittest.TestCase):

    def setUp(self) -> None:
        self.environment = Environment()
        for name in ("Alice", "Bob"):
            AutonomousAgent(name)

    def tearDown(self) -> None:
        self.environment.agents.clear()  # the environment is a singleton, hence we must empty the agent container

    @property
    def agents(self):
        return self.environment.agents.values()

    def test_presence_two_agents(self):
        self.assertEqual(len(self.environment.agents), 2)

    def test_can_find_other_agent(self):
        alice, bob = self.agents
        self.assertEqual(alice, bob.other_agents.pop())

    def test_register_message_handler(self):
        alice, bob = self.agents
        message_handler = FilterMessagesByWordAndPrint("hello")
        alice.register_message_handler(message_handler)
        self.assertEqual(alice.message_handlers[0], message_handler)

    def test_message_sending_behavior(self):
        alice, bob = self.agents

        create_spam = GenerateRandom2WordMessage(vocabulary=['test', 'test'])
        send_messages = SendOutgoingMessages()

        self.assertTrue(bob.inbox.empty())
        alice.register_behavior(create_spam)
        alice.register_behavior(send_messages)

        self.assertTrue(bob.inbox.empty())
        self.environment.event_loop.run_until_complete(self.environment.run())
        self.assertFalse(bob.inbox.empty())
