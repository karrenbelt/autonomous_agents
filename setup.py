import sys
import platform
from distutils.core import setup

VERSION = '0.1.0'
CURRENT_PYTHON = sys.version_info[:2]
REQUIRED_PYTHON = (3, 8)

if platform.system() != 'Linux':
    sys.stdout.write(f"seems you like living on the edge, detected: {platform.system()}")

if CURRENT_PYTHON < REQUIRED_PYTHON:
    sys.stderr.write("unsupported python version: {}.{} should be {}.{}".format(*CURRENT_PYTHON + REQUIRED_PYTHON))

setup(name='agents',
      version=VERSION,
      description='',
      python_requires='>={}.{}'.format(*REQUIRED_PYTHON),
      author='M.A.P. Karrenbelt',
      author_email='m.a.p.karrenbelt@gmail.com',
      url='https://gitlab.com/karrenbelt/autonomous_agents',
      packages=[],
      install_requires=[],
      dependency_links=[]
      )
